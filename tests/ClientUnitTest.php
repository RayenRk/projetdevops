<?php
namespace App\Tests\Entity;

use App\Entity\Client;
use PHPUnit\Framework\TestCase;

class ClientTest extends TestCase
{
    public function testClientEntity()
    {
        // Create a Client entity
        $client = new Client();
        
        // Set some values for testing
        $client->setNom('Rayen Rakkad');
        $client->setCin('11223344');
        
        // Assert that getters return the correct values
        $this->assertEquals('Rayen Rakkad', $client->getNom());
        $this->assertEquals('11223344', $client->getCin());

        
    }
}

