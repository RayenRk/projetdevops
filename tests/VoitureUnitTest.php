<?php


namespace App\Tests\Entity;

use App\Entity\Voiture;
use PHPUnit\Framework\TestCase;

class VoitureTest extends TestCase
{
    public function testVoitureEntity()
    {
        // Create a Voiture entity
        $voiture = new Voiture();

        // Set some values for testing
        $voiture->setModele('A8');
        $voiture->setMarche('128100');

        // Assert that getters return the correct values
        $this->assertEquals('A8', $voiture->getModele());
        $this->assertEquals('128100', $voiture->getMarche());

        
        $annee = '2024';
        $voiture->setDateMise($annee);
        $this->assertEquals($annee, $voiture->getDateMise());

        
    }
}

